FactoryBot.define do
  factory :user do
    name { "Test User" }
    email { "test@example.com" }
    password { "test123" }
  end
end
